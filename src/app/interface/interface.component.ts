import { Component } from '@angular/core';

@Component({
    selector: 'interface',
    templateUrl: './interface.component.html',
    styleUrls: ['./interface.component.css']
})

export class InterfaceComponent {
    nombre:string;
    nombre_mejor_juego:string;
    nombre_mejor_juego_retro:string;
    mostrar_retro:boolean;
    color_background:string;
    year:number;
    public videojuegos:Array<string>;

    constructor(){
        this.nombre = 'Helsereiser';
        this.nombre_mejor_juego = 'GTA 5';
        this.nombre_mejor_juego_retro = 'Super Mario Bros';
        this.mostrar_retro = true;
        this.color_background = "yellow";
        this.year = 2018;
        this.videojuegos  = [
            'videojuego 1',
            'videojuego 2',
            'videojuego 3',
            'videojuego 4',
            'videojuego 5'
        ];
    }

}